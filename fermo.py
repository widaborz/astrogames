from sense_hat import SenseHat
import time
from datetime import datetime, timedelta

sense = SenseHat()
sense.clear()  # Blank the LED matrix
sense.set_rotation(270)  # Flight orientation
sense.clear()


ORANGE = (249, 115, 6)
GREEN = (0, 255, 0)
RED = (255, 0, 0)


sense.show_message("Cammina mantenendo l'orologio alla stella altezza", 0.05)
sense.show_message("3   2   1   GO!", 0.05)


game_duration = 10  # Durata del gioco in secondi
timeout = 120  # Timeout in secondi (2 minuti)
start_time = time.time()
end_game = datetime.now() + timedelta(seconds=timeout)

while True:
    elapsed_time = time.time() - start_time
    print(elapsed_time)

    # Controllo del tempo di gioco
    if elapsed_time >= game_duration:
        sense.clear(GREEN)
        print("VINTO")
        break

    # Controllo del timeout
    if datetime.now() >= end_game:
        sense.clear(RED)
        print("Timeout")
        break

    # Lettura dei valori dell'accelerometro
    acceleration = sense.get_accelerometer_raw()
    x = acceleration['x']
    y = acceleration['y']
    z = acceleration['z']

    # Controllo del movimento
    if  z > 1:
        print("Movimento rilevato!")
        sense.clear(RED)  # Mostra schermata rossa
        start_time = time.time()  # Reset del timer
    else:
        sense.clear(ORANGE)  # Mostra schermata verde

    time.sleep(0.1)


time.sleep(2)
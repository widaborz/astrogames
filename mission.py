from sense_hat import SenseHat
import random
import time

sense = SenseHat()
sense.set_rotation(270)

B = (0, 0, 0)
W = (255, 255, 255)
O = (249, 115, 6)
R = (255, 0, 0)
G = (255, 255, 0)
A = (0,0,255) 

sense.show_message("Preparati ad andare in missione", 0.05)

sense.show_message("3   2   1   GO!", 0.05)

# Definisci le immagini dei pianeti
giove = [
    B, B, B, O, O, B, B, B,
    B, B, O, O, O, O, B, B,
    B, O, O, O, O, O, O, B,
    B, O, O, O, R, O, O, B,
    B, O, O, O, R, O, O, B,
    B, B, O, O, O, O, B, B,
    B, B, B, O, O, B, B, B,
    B, B, B, B, B, B, B, B
]

marte = [
    B, B, B, B, B, B, B, B,
    B, B, B, R, R, B, B, B,
    B, B, R, R, O, R, B, B,
    B, B, R, O, R, R, B, B,
    B, B, B, R, R, B, B, B,
    B, B, B, B, B, B, B, B,
    B, B, B, B, B, B, B, B,
    B, B, B, B, B, B, B, B
]

saturno = [
    B, B, B, B, B, B, B, B,
    B, B, B, O, O, B, B, B,
    B, B, O, O, O, O, B, B,
    W, W, W, W, W, W, W, W,
    B, B, O, O, O, O, B, B,
    B, B, B, O, O, B, B, B,
    B, B, B, B, B, B, B, B,
    B, B, B, B, B, B, B, B
]

nettuno = [
    B, B, B, B, B, B, B, B,
    B, B, B, B, B, B, B, B,
    B, B, A, A, B, B, B, B,
    B, A, A, A, A, B, B, B,
    B, A, A, A, A, B, B, B,
    B, B, A, A, B, B, B, B,
    B, B, B, B, B, B, B, B,
    B, B, B, B, B, B, B, B
]

sense.set_pixels(giove)
time.sleep(2)
sense.set_pixels(marte)
time.sleep(2)
sense.set_pixels(saturno)
time.sleep(2)
sense.set_pixels(nettuno)
time.sleep(2)
sense.show_message("Vai in missione su: ", 0.05)

# Lista delle immagini dei pianeti
pianeti = [giove, marte, saturno, nettuno]

# Funzione per visualizzare una delle immagini dei pianeti in modo casuale
def mostra_pianeta():
    indice_pianeta = random.randint(0, 3)
    immagine_pianeta = pianeti[indice_pianeta]
    sense.set_pixels(immagine_pianeta)


# Mostra una delle immagini dei pianeti
mostra_pianeta()

# Attendi per qualche secondo
time.sleep(30)
sense.clear()
import RPi.GPIO as GPIO
from sense_hat import SenseHat
import time
import subprocess
import os
 
sense = SenseHat()
sense.clear()  # Blank the LED matrix
sense.set_rotation(270)  # Flight orientation
sleep_time = 0.3

# Definisci i numeri dei pin GPIO collegati ai pulsanti
pin_pulsanti = [13, 19, 26, 16, 20, 21]

# Imposta la modalità dei pin GPIO
GPIO.setmode(GPIO.BCM)

# Configura i pin GPIO come input con pull-up interna
for pin in pin_pulsanti:
    GPIO.setup(pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)

# Variabile di controllo per evitare avvio ripetuto di "salto.py"
salto_avviato = False
fermo_avviato = False
debris_avviato = False
mission_avviato = False

sense.show_message("Astrogame v.0.0.1", 0.05)

current_dir = os.path.dirname(os.path.abspath(__file__))

# Funzione di callback per la pressione dei pulsanti
def pulsante_premuto(channel):
    global salto_avviato
    global fermo_avviato
    global debris_avviato
    global mission_avviato

    if channel == 26 and not salto_avviato:
        # Gioco 1
        print("gioco 1")
        sense.show_letter("1")
        salto_path = os.path.join(current_dir, 'salto.py')
        subprocess.run(['python', salto_path])
        sense.clear()
        sense.show_message("Vai al punto 2", 0.05)
        salto_avviato = True
    if channel == 20 and not fermo_avviato:
        # Gioco 2
        print("gioco 2")
        sense.show_letter("2")
        fermo_path = os.path.join(current_dir, 'fermo.py')
        subprocess.run(['python', fermo_path])
        sense.clear()
        sense.show_message("Vai al punto 3", 0.05)
        fermo_avviato = True
    if channel == 13 and not debris_avviato:
        # Gioco 3
        print("gioco 3")
        sense.show_letter("3")
        debris_path = os.path.join(current_dir, 'debris.py')
        subprocess.run(['python', debris_path])
        sense.clear()
        sense.show_message("Vai al punto 4", 0.05)
        debris_avviato = True
    if channel == 19 and not mission_avviato:
        # Gioco 4
        print("gioco 4")
        sense.show_letter("4")
        mission_path = os.path.join(current_dir, 'mission.py')
        subprocess.run(['python', mission_path])
        sense.clear()
        mission_avviato = True
    if channel == 16:
        # OK
        print("ok")
        sense.show_letter("o")
        time.sleep(sleep_time)
        sense.clear()
    if channel == 21:
        # Reset
        print("reset")
        sense.show_letter("r")
        time.sleep(sleep_time)
        sense.clear()
        salto_avviato = False
        fermo_avviato = False
        debris_avviato = False
        mission_avviato = False


# Imposta la callback per ogni pulsante
for pin in pin_pulsanti:
    GPIO.add_event_detect(pin, GPIO.FALLING, callback=pulsante_premuto, bouncetime=400)

# Loop principale per mantenere lo script in esecuzione
try:
    while True:
        pass

# Interrompi il loop e ripulisci i pin GPIO alla chiusura dello script
except KeyboardInterrupt:
    GPIO.cleanup()

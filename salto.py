from sense_hat import SenseHat
from datetime import datetime, timedelta
import time

# Inizializza l'oggetto SenseHat
sense = SenseHat()
sense.clear()
sense.set_rotation(270)
sensitivity = 100
limite = 350
accelerazione = 0
time_limit = 30

future_date = datetime.now() + timedelta(seconds=time_limit)
print(datetime.now())
print(future_date)

black = (0, 0, 0)
green = (0, 255, 0)
ORANGE = (249, 115, 6)

sense.show_message("Salta finchè lo schermo non diventa verde", 0.05)

sense.show_message("3   2   1   GO!", 0.05)



def matrice(numero) : 
    if numero >= limite :
        a = green
        b = green
        c = green
        d = green
        e = green
        f = green
        g = green
        h = green
    elif numero >= (limite/8)*7 :
        a = black
        b = ORANGE
        c = ORANGE
        d = ORANGE
        e = ORANGE
        f = ORANGE
        g = ORANGE
        h = ORANGE
    elif numero >= (limite/8)*6 :
        a = black
        b = black
        c = ORANGE
        d = ORANGE
        e = ORANGE
        f = ORANGE
        g = ORANGE
        h = ORANGE
    elif numero >= (limite/8)*5 :
        a = black
        b = black
        c = black
        d = ORANGE
        e = ORANGE
        f = ORANGE
        g = ORANGE
        h = ORANGE
    elif numero >= (limite/8)*4 :
        print("numero >= limite")
        print(numero)
        a = black
        b = black
        c = black
        d = black
        e = ORANGE
        f = ORANGE
        g = ORANGE
        h = ORANGE
    elif numero >= (limite/8)*3 :
        a = black
        b = black
        c = black
        d = black
        e = black
        f = ORANGE
        g = ORANGE
        h = ORANGE
    elif numero >= (limite/8)*2 :
        a = black
        b = black
        c = black
        d = black
        e = black
        f = black
        g = ORANGE
        h = ORANGE
    else : 
        a = black
        b = black
        c = black
        d = black
        e = black
        f = black
        g = black
        h = ORANGE

    image = [
        a, a, a, a, a, a, a, a, 
        b, b, b, b, b, b, b, b,
        c, c, c, c, c, c, c, c, 
        d, d, d, d, d, d, d, d, 
        e, e, e, e, e, e, e, e, 
        f, f, f, f, f, f, f, f, 
        g, g, g, g, g, g, g, g,
        h, h, h, h, h, h, h, h
    ]

    return image

def leggi_salto() : 
    salto = 0
    for i in range(50):
        accelerometro = sense.get_accelerometer_raw()
        if salto < accelerometro['z'] : 
            salto = accelerometro['z']
    
    return salto


while accelerazione < limite and datetime.now() < future_date :
    # Leggi i dati dell'accelerometro
    accelerometro = sense.get_accelerometer_raw()
    
    # Estrai i valori dell'asse x, y e z
    x = accelerometro['x']
    y = accelerometro['y']
    z = accelerometro['z']
    
    salto = 0
    # Controlla se il Sense HAT è in movimento
    if abs(x) > 1 or abs(y) > 1 or abs(z) > 1:
        sense.clear()
        salto = round(abs(leggi_salto())*sensitivity, 0)
        print("Valore dell'accelerometro:", salto)

        if accelerazione < salto : 
            accelerazione = salto
    else : 
        sense.set_pixels(matrice(accelerazione))

if accelerazione >= limite : 
    sense.set_pixels(matrice(accelerazione))
    time.sleep(2)


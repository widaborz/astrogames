from sense_hat import SenseHat
import time
import random

sense = SenseHat()
sense.set_rotation(270)  # Flight orientation

sense.show_message("Cattura i detriti spaziali", 0.05)

sense.show_message("3   2   1   GO!", 0.05)

# Imposta i colori
colore_oggetto = (255, 0, 0)  # Rosso
colore_sfondo = (0, 0, 0)  # Nero
colore_giocatore = (255, 255, 255)  # Bianco
GREEN = (0, 255, 0)

# Imposta la velocità del gioco
velocita_gioco = 0.05

# Variabili di gioco
punteggio = 0
tempo_iniziale = time.time()



# Posizione iniziale del giocatore
posizione_giocatore = [2, 2]

# Funzione per generare nuovi oggetti
def genera_oggetto():
    x = random.randint(0, 7)
    y = random.randint(0, 7)
    return [x, y]

# Genera il primo oggetto
oggetto = genera_oggetto()

# Funzione per aggiornare il gioco
def aggiorna_gioco():
    global punteggio, oggetto, posizione_giocatore

    # Pulisci la matrice di gioco
    matrice_gioco = [[colore_sfondo for _ in range(8)] for _ in range(8)]

    # Disegna l'oggetto
    matrice_gioco[oggetto[0]][oggetto[1]] = colore_oggetto

    # Disegna il giocatore
    for x in range(2):
        for y in range(2):
            matrice_gioco[posizione_giocatore[0] + x][posizione_giocatore[1] + y] = colore_giocatore

    # Aggiorna la matrice di gioco
    sense.set_pixels(sum(matrice_gioco, []))

# Loop di gioco
while True:
    # Aggiorna il gioco
    aggiorna_gioco()

    # Controlla l'input dell'accelerometro
    accelerometro = sense.get_accelerometer_raw()
    x = accelerometro['x']
    y = accelerometro['y']

    # Muovi il giocatore
    if x > 0.1 and posizione_giocatore[0] < 6:
        posizione_giocatore[0] += 1
    elif x < -0.1 and posizione_giocatore[0] > 0:
        posizione_giocatore[0] -= 1

    if y > 0.1 and posizione_giocatore[1] < 6:
        posizione_giocatore[1] += 1
    elif y < -0.1 and posizione_giocatore[1] > 0:
        posizione_giocatore[1] -= 1

    # Raccogli l'oggetto se viene colpito
    if oggetto[0] >= posizione_giocatore[0] and oggetto[0] < posizione_giocatore[0] + 2 and \
            oggetto[1] >= posizione_giocatore[1] and oggetto[1] < posizione_giocatore[1] + 2:
        punteggio += 1
        oggetto = genera_oggetto()

    # Controlla la condizione di fine gioco
    tempo_trascorso = time.time() - tempo_iniziale
    if punteggio == 10 or tempo_trascorso >= 120:
        break

    # Attendi un po' prima dell'aggiornamento successivo
    time.sleep(velocita_gioco)

# Mostra il punteggio finale
sense.clear(GREEN)
time.sleep(2)
sense.clear()
